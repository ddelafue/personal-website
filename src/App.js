import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Routes from './routes';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
   
const styles = theme => ({
		root: {
				flexGrow:  1,
				alignItems: 'center',
				backgroundColor: '#D8D8D8',
				height: '100%'
		}
})
const App = (props) => (
			<div className={props.classes.root}>
							<AppBar position="static" color="default" style={{backgroundColor: 'transparent', height: '5vh'}}>
					<Toolbar>
						<Typography variant="title" color="inherit">
							Hi
						</Typography>
					</Toolbar>
			</AppBar> 
				<Grid
					className="fullGrid"
					container
					alignItems="center"
					justify="center"
 					spacing={0}>
						<Grid className="center" item xs={12}>
							<Routes />
						</Grid>
				</Grid>
			</div>

)



export default withStyles(styles) (App);



