import React from 'react';
import {BrowserRouter, Route, Switch } from 'react-router-dom'
import Home from './Home/Home.js';
import About from './About/About.js'; 
import Projects from './Projects/Projects.js';
import Contact from './Contact/Contact.js';
export default () =>
		(
				<BrowserRouter>
					<Switch>
						<Route path="/" exact render={props => <Home {...props} />}/>
						<Route path="/About" exact render={props => <About {...props} />}/>
						<Route path="/Contact" exact render={props => <Contact {...props}/>}/>
						<Route path="/Projects" exact render={props => <Projects {...props}/>}/>
				
				</Switch>
				</BrowserRouter>
		);
