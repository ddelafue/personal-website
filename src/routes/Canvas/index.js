import React from 'react';
//import Circle from 'react-art/Circle';
import Circle from '../../components/Shapes/Circle.js';
import { Surface, Text } from 'react-art';
import { Component  } from 'react';
import Rectangle from '../../components/Shapes/Rectangle.js';
import { withRouter  } from "react-router-dom";
class Canvas extends Component {
		constructor(){
				super();
				this.state = {
						x1Initial: 0,
						y1Initial: 0,
						x1Current: 0,
						y1Current: 0,
						x2Initial: 0,
						y2Initial: 0,
						y2Current: 0,
						x2Current: 0,
						x3Current: 0,
						y3Current: 0,
						x4Current: 0,
						y4Current: 0,
						currentWidth: 0,
						count: 0,
						time: 0
				}
		}
		componentWillMount() {
			let x1Initial, y1Initial, x1Current, y1Current, x2Initial, y2Initial, y2Current, x2Current, currentWidth;
			x1Initial= x2Initial = this.props.width/2;
			y1Initial = y2Initial = this.props.height - (this.props.height/8);
			x1Current, x2Current = x1Initial;
			y1Current, y2Current = y1Initial;
			currentWidth = this.props.width/256;
			let count = 0;
			this.setState({ x1Initial, y1Initial, x1Current, y1Current, x2Initial, y2Initial, x2Current, y2Current, currentWidth, count });
		}
		componentDidMount() {
				setInterval(() => {
						if(this.props.width != 0) {
							if(this.state.x1Initial === 0 && this.state.y1Initial === 0) {
								let x1Initial = this.props.width/2;
								let y1Initial = this.props.height - (this.props.height/8);
								let x1Current = x1Initial;
								let y1Current = y1Initial; 
								let x2Initial = x1Initial;
								let y2Initial = y1Initial;
								let x2Current = x1Current;
								let y2Current = y1Current;
								
								let x3Current = x1Current;
								let y3Current = y1Current;
								let x4Current = x1Current;
								let y4Current = y1Current;
								let currentWidth = this.props.width/256;
								let count = 0;
								console.log("initialized to width");
								this.setState({ x1Initial, y1Initial, x1Current, y1Current, x2Initial, y2Initial, x2Current, y2Current, x3Current, y3Current, x4Current, y4Current, currentWidth, count});
							} else {
								let time = this.state.time + .001;

								let count = this.state.count+ 1;
								let currentWidth = this.state.currentWidth;
								if(count < 256)
									{
											currentWidth += this.props.width/256;
									}
								let radius = this.props.height/4 + ((this.props.height/2 - 10) - (this.props.height/4))/2;
								let deltax = radius * Math.cos(time);
								let deltay = radius * Math.sin(time);
								let deltax2 = radius * Math.cos(time +1.5708);
								let deltay2 = radius * Math.sin(time + 1.5708);
								let deltax3 = radius * Math.cos(time +3.14159);
								let deltax4 = radius * Math.cos(time +4.71239);
								let deltay3 = radius * Math.sin(time + 3.14159);
								let deltay4 = radius * Math.sin(time + 4.71239);
									//let x1Current = this.state.x1Initial + deltax;
									//let y1Current = this.state.y1Initial + deltay;
								let x1Current = this.props.width/2 + deltax;
								let y1Current = this.props.height/2 +deltay;
								let x2Current = this.props.width/2 + deltax2;
								let y2Current = this.props.height/2 + deltay2;

								let x3Current = this.props.width/2 + deltax3;
								let y3Current = this.props.height/2 +deltay3;
								let x4Current = this.props.width/2 + deltax4;
								let y4Current = this.props.height/2 + deltay4;

								this.setState({ x1Current, y1Current, x2Current, y2Current, x3Current, y3Current, x4Current, y4Current, time, currentWidth, count }); 

							} 
					 	} }, 1);
		
		}
		render() {
				console.log("this.props.width is " +this.props.width);
				console.log("a is "+this.state.a);
				if (this.props.width != 0 && this.state.x1Initial != 0)
				{
					console.log('rendering canvas');
					return(
					   <Surface ref= "surface" width={this.props.width} height={this.props.height} alignItems= {'center'}>
							<Circle
								setX={this.props.width/2}
								setY={this.props.height/2}
								radius={this.props.height/4}
								stroke="#A9D0F5"
								strokeWidth={10}
								fill="#FFFF"
								change= { () => this.props.history.push('/About') }
							/>
							<Circle
								setX={this.props.width/2}
								setY={this.props.height/2}
								radius={this.props.height/2 - 10}
								stroke="#0489b1"
								strokeWidth={5}
								change= { () => this.props.history.push('/About') }
							/>

							
						    <Circle
								setX={this.state.x1Current}
								setY={this.state.y1Current}
								radius ={this.props.height/16 }
								stroke="black"
								strokeWidth={1}
								fill="black"
								change= { () => this.props.history.push('/About') }
					
							/>

						    <Circle
								setX={this.state.x2Current}
								setY={this.state.y2Current}
								radius ={this.props.height/16 }
								stroke="black"
								strokeWidth={1}
								fill="black"
								change= { () => this.props.history.push('/Projects') }
							/>

							<Circle
								setX={this.state.x3Current}
								setY={this.state.y3Current}
								radius ={this.props.height/16 }
								stroke="black"
								strokeWidth={1}
								fill="black"
								change= { () => this.props.history.push('/About') }
				
							/>

							<Circle
								setX={this.state.x4Current}
								setY={this.state.y4Current}
								radius ={this.props.height/16 }
								stroke="black"
								strokeWidth={1}
								fill="black"
								change= { () => this.props.history.push('/Contact') }
							/>
							
							<Rectangle
								width={this.props.width/64}
								height={this.props.height}
								fill="#D8D8D8"
								stroke="black"
								strokeWidth={1}
							/>
							<Rectangle
									xOffset={this.props.width/128 - this.props.width/512}
								yOffset={10}
								width={this.props.width/256}
								height={this.props.height - 20}
								fill="white"
								stroke="black"
								strokeWidth={1}
							/>

					
							<Text x={this.state.x1Current} y={this.state.y1Current} alignment="middle" fill="#fff" font='20px bold '>About</Text>
							<Text x={this.state.x2Current} y={this.state.y2Current} alignment="middle" fill="#fff" font='20px bold'>Projects</Text>
							<Text x={this.state.x3Current} y={this.state.y3Current} alignment="middle" fill="#fff" font='20px bold'>Interests/Hobbies</Text>
							<Text x={this.state.x4Current} y={this.state.y4Current} alignment="middle" fill="#fff" font='20px bold'>Contact</Text>

					</Surface>);

				}	
				else 
				{ 
						return(<div>a</div>); 
				}
		}


}



export default withRouter(Canvas);

/*<Circle
								setX={this.props.width/2}
								setY={this.props.height/8}
								radius ={this.props.height/16 }
								stroke="black"
								strokeWidth={1}
								fill="black"
								change={null}
							/> 

							<Circle
									setX={(this.props.width*7/8) - this.props.height/8}
								setY={this.props.height/2}
								radius ={this.props.height/16 }
								stroke="black"
								strokeWidth={1}
								fill="black"
								change={null}
							/>
							*/
