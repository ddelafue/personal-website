import React from 'react';
//import Circle from 'react-art/Circle';
import Circle from '../../components/Shapes/Circle.js'
import { Surface } from 'react-art';
import { Component  } from 'react';
import Canvas from '../Canvas';
import Diego from '../../images/1.png';
class Home extends Component {
		constructor(){
				super();
				this.state = {
						width: 0,
						height: 0
				}
				console.log(this.state.width + "is the width");
				console.log(this.state.height + "is the height");
	
		}
		componentWillMount() {

		}
		componentDidMount() {
					const width = this.refs.child.parentNode.clientWidth;
					const height = this.refs.child.parentNode.clientHeight;
					console.log('width changed to: '+ width);
					console.log('height changed to' + height);
					this.setState({ width, height });
		}
		render () {
			const widthSet = this.width;
			console.log('widthSet is '+ widthSet);
			console.log('on mount, width is ' + this.state.width)
			return (	
					<div ref="child" style={{display: 'flex', height: '95vh', justifyContent: 'center'}}>
						<div class="inner" style={{ display: 'flex' , justifyContent: 'center', alignItems: 'center', zIndex: '1', top: '50%', textAlign: 'center'}}>
							<img width="250" height="250" align="middle" src={Diego} />
						</div>
						<div class="inner">
							<Canvas width={this.state.width} height={this.state.height} />
						</div>
					</div>
			);
		}
}





export default Home;
